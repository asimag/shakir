<div class="box box-info" style="padding-bottom: 20px">
    <div class="box-header with-border">
        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 10px">
                <a href="admin/" class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>

    {!! Form::open(['url' => 'admin/transactions', 'class' => 'form-horizontal', 'method' => 'GET']) !!}
        <div class="box-body">
           <div class="fields-group">
                <div class="form-group ">
                    {{ Form::label('project', 'Select Project : ', array('class' => 'control-label  col-sm-4')) }}
                    <div class="col-sm-8">
                        <div class="input-group projects_area">
                            {{Form::select('project', $projects, null, array('class' => 'form-control'))}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="fields-group">
                <div class="form-group ">
                    {{ Form::label('vendor', 'Select Vendor : ', array('class' => 'control-label  col-sm-4')) }}
                    <div class="col-sm-8">
                        <div class="input-group vendors_area">
                            {{Form::select('vendor', $vendors, null, array('class' => 'form-control'))}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="fields-group">
                <div class="form-group ">
                    {{ Form::label('product', 'Select Product : ', array('class' => 'control-label  col-sm-4')) }}
                    <div class="col-sm-8">
                        <div class="input-group products_area">
                            {{Form::select('product', $products, null, array('class' => 'form-control'))}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fields-group">
            <div class="form-group ">
                {{ Form::label('date_range', 'Date Range : ', array('class' => 'control-label col-sm-4')) }}
                <div class="col-sm-8">
                    <div class="input-group date_range_area">
                        {{ Form::text('date_range', old('date_range'), array('class' => 'form-control daterange')) }}
                    </div>
                </div>
            </div>
        </div>
    <div class="fields-group all_dates_area @if ($errors->has('all_dates')) has-error @endif">
        <div class="form-group">

            {{ Form::label('all_dates', 'All Dates : ', array('class' => 'control-label pregnancy_report_type col-sm-4')) }}
            <div class="col-sm-8">
                <div class="input-group">
                    {{ Form::checkbox('all_dates', null, null, array()) }}
                </div>
            </div>

            @if ($errors->has('all_dates'))
                <span class="help-block">{{ $errors->first('all_dates') }}</span>
            @endif
        </div>
    </div>
    <div class="fields-group">
        <div class="form-group">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
            </div>
        </div>

    </div>

    {!! Form::close() !!}
</div>
<br>

