function abc() {
    $('input[name="date_range"]').daterangepicker( {"linkedCalendars": false});
    $("#ext").prop("disabled", true);
    $('#qty').change(function()
    {
        var qty = $('#qty').val();
        var currency = $('#price').val();
        var price = Number(currency.replace(/[^0-9\.-]+/g,""));
        $("#ext").val(qty*price);

    });

    $('#price').change(function()
    {
        var qty = $('#qty').val();
        var currency = $('#price').val();
        var price = Number(currency.replace(/[^0-9\.-]+/g,""));
        $("#ext").val(qty*price);

    });

}

