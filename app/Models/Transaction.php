<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'Transactions';

    public function Product()
    {
        return $this->belongsTo('App\Models\Product');
    }
    public function Vendor()
    {
        return $this->belongsTo('App\Models\Vendor');
    }
    public function Project()
    {
        return $this->belongsTo('App\Models\Project');
    }

}
