<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'Products';

    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor');
    }
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }


}
