<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'Vendors';

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

}
