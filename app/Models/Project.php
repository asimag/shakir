<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'Projects';

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }
}
