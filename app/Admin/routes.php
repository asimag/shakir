<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index');
    $router->get('/reports', 'ReportController@index');


    $router->resource('vendors', VendorController::class);
    $router->resource('products', ProductController::class);
    $router->resource('projects', ProjectController::class);
    $router->resource('transactions', TransactionController::class);
    $router->resource('secure-transactions', SecureTransactionController::class);

    $router->get('/api/products', 'TransactionController@products');
});
