<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use App\Models\Transaction;

use App\Models\Vendor;
use Carbon\Carbon;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    use ModelForm;
    public static $total;
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index(Request $request)
    {
        return Admin::content(function (Content $content) use ($request) {
            $content->header('Transaction');
            $content->description('description');

            $content->body($this->grid($request));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Transaction');
            $content->description('edit');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Transaction');
            $content->description('create');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(Request $request)
    {
        return Admin::grid(Transaction::class, function (Grid $grid) use ($request) {
            $date_ranges = explode(" - ", $request->input('date_range'));
            $all_dates_flag = $request->input('all_dates');

            if (count($date_ranges) > 1 && !$all_dates_flag) {
                $date_ranges[0] = Carbon::createFromTimestamp(strtotime(trim($date_ranges[0])))->toDateTimeString();
                $date_ranges[1] = Carbon::createFromTimestamp(strtotime(trim($date_ranges[1])))->toDateTimeString();
                $grid->model()->whereBetween('transaction_date', [ $date_ranges[0],  $date_ranges[1]]);
            }
            $project_id = $request->input('project');
            $vendor_id = $request->input('vendor');
            $product_id = $request->input('product');
            if ($vendor_id) {
                $grid->model()->where('vendor_id', $vendor_id);
            }
            if ($project_id) {
                $grid->model()->where('project_id', $project_id);
            }
            if ($product_id) {
                $grid->model()->where('product_id', $product_id);
            }
            $grid->paginate(100);
            $grid->perPages([50, 100, 150, 200, 500]);
            $grid->id('ID')->sortable();
            $grid->transaction_date();
            $grid->description();
            $grid->project()->name('Project');
            $grid->product()->name('Product');
            $grid->vendor()->name('Vendor');
            $grid->qty();
            $grid->price();
            $grid->ext();
    
            $grid->column("Total")->display(function () {
                self::$total += $this->ext;
                return self::$total;
            });
            $grid->paid()->display(function ($paid) {
                if ($paid == 0) {
                    return 'Not Paid';
                } else {
                    return 'Paid';
                }
            });
            $grid->filter(function ($filter) {
                $filter->equal('project_id', "Project Id");
                $filter->equal('product_id', "Product Id");
                $filter->equal('paid', "Paid")->select(['1' => "Paid", '0' => 'Not Paid']);
             
                $filter->like('description', "Description");
                $filter->between('transaction_date', 'Transaction Date');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Transaction::class, function (Form $form) {
            $form->display('id', 'ID');
            $form->date('transaction_date', 'Date')->rules('required');
            $form->text('description', 'Description');
            $products = \App\Models\Product::all()->pluck('name', 'id');
            $form->select('product_id', "Product/Cash/Bank")->options(function ($id) {
                $product = Product::with('vendor')->find($id);
                if ($product) {
                    return [$product->id => $product->vendor->name . " : " . $product->name];
                }
            })->ajax('/admin/api/products');
            ;
            $projects = \App\Models\Project::all()->pluck('name', 'id');
            $form->select('project_id', "Project")->options($projects)->rules('required');
            ;
            $form->text('qty', 'Qty')->rules('required');
            ;
            $form->hidden('vendor_id');
            $form->currency('price', 'Price')->symbol('Rs.')->rules('required');
            ;
            $form->text('ext', 'Total Amount');
            $states = [
                'on'  => ['value' => 1, 'text' => 'Paid', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Not Paid', 'color' => 'danger'],
            ];
            
            $form->switch('paid')->states($states);
            

            $form->saving(function (Form $form) {
                $form->vendor_id = Product::find($form->product_id)->vendor_id;
                $form->ext = $form->qty * $form->price;
            });
        });
    }

    public function products(Request $request)
    {
        $q = $request->get('q');

        return Product::where('Products.name', 'like', "%$q%")->leftJoin('Vendors', 'Products.vendor_id', '=', 'Vendors.id')->select(DB::raw("Products.id, CONCAT(Products.name,' : ', Vendors.name) AS text"))
        ->paginate(null, ['Products.id', 'text']);
    }
}
