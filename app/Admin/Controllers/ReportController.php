<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Project;
use App\Models\Vendor;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class ReportController extends Controller
{
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Report');
            $content->description('Description...');

            $content->row($this->reportForm());

       
        });
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reportForm()
    {
        $projects = Project::pluck('name', 'id')->prepend('All', 0);
        $vendors = Vendor::pluck('name', 'id')->prepend('All', 0);
        $products = Product::pluck('name', 'id')->prepend('All', 0);

        return view('trx_report', compact('projects', 'vendors', 'products'));
    }
}
